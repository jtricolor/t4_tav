package Utils;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;

public class JodaTimeUtils {
	
	private int idade;
	private DateMidnight hoje;
	
	public JodaTimeUtils(){
		setHoje();
	}
	
	public int calculaIdade(DateTime dataNascimento){ 
		Period period = new Period(dataNascimento,DateTime.now());
		return period.get(DurationFieldType.years());
	}
	public int getIdade() {
		return idade;
	}

	public DateMidnight getHoje() {
		return hoje;
	}
	
	public void setHoje() {
		this.hoje = new DateMidnight();
	}
}
