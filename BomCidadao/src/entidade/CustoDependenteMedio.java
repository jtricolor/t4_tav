package entidade;

import java.math.BigDecimal;

public class CustoDependenteMedio extends CustoDependente {
	public CustoDependenteMedio(){
		super();
		this.setValor(new BigDecimal(55));
	}
}
