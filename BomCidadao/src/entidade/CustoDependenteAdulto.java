package entidade;

import java.math.BigDecimal;

public class CustoDependenteAdulto extends CustoDependente{
	public CustoDependenteAdulto(){
		super();
		this.setValor(new BigDecimal(150));
	}
}
