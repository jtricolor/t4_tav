package entidade;

import java.math.BigDecimal;

public abstract class CustoDependente {
	private BigDecimal valor;

	public static final CustoDependente instanciaCustoporIdade(int idade){
		if( idade<21 ) {
			return new CustoDependenteJovem();
		} else if( idade<35 ) {
			return new CustoDependenteMedio();
		} else if( idade<65 ) {
			return new CustoDependenteAdulto();
		} else {
			return new CustoDependenteSenior();
		}
	}
	
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
}
