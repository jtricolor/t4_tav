package entidade;

import java.math.BigDecimal;
import java.util.List;

public class Titular {
	
	private List<Dependente> dependentes;
	private Integer matricula;
	private String nome;
	
	
	public Titular(Integer matricula, String nome) {
		super();
		setMatricula(matricula);
		setNome(nome);
	}
	
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		if(matricula==null) throw new IllegalArgumentException("Matr�cula n�o pode ser nula.");
		this.matricula = matricula;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		if(nome==null || nome.length()<3) throw new IllegalArgumentException("Nome inv�lido");
		this.nome = nome;
	}

	public List<Dependente> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<Dependente> dependentes) {
		if(dependentes==null) throw new IllegalArgumentException("Dependentes inv�lidos");
		this.dependentes = dependentes;
	}

	public BigDecimal calcularCustoDependentes() {
		BigDecimal total = BigDecimal.ZERO;
		if(dependentes==null){
			return total;
		}
		for(Dependente dependente : getDependentes()) {
			total=total.add(dependente.getValor());
		}
		return total;
	}
	
}