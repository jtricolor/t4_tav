package entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import Utils.JodaTimeUtils;

public class Dependente {

	private String nome;
	private int idade;
	private CustoDependente custo;
	
	public Dependente(String nome, DateTime dataNascimento) {
		super();
		setNome(nome);
		setIdade(new JodaTimeUtils().calculaIdade(dataNascimento));
		setCusto();
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		if(nome==null || nome.length()<3) throw new IllegalArgumentException("Nome inv�lido");
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public CustoDependente getCusto() {
		return custo;
	}

	public void setCusto() {
		this.custo = CustoDependente.instanciaCustoporIdade(getIdade());
	}
	
	public BigDecimal getValor(){
		return getCusto().getValor();
	}
}
