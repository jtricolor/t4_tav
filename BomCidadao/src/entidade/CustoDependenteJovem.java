package entidade;

import java.math.BigDecimal;

public class CustoDependenteJovem extends CustoDependente {

	public CustoDependenteJovem(){
		super();
		this.setValor(new BigDecimal(22));
	}
	
}
