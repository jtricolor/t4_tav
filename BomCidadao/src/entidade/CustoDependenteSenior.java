package entidade;

import java.math.BigDecimal;

public class CustoDependenteSenior extends CustoDependente {
	public CustoDependenteSenior(){
		super();
		this.setValor(new BigDecimal(320));
	}
}
