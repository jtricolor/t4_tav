package tests.entidade;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Test;

import entidade.CustoDependenteMedio;
import entidade.Dependente;

public class TestaDependente {
	
	@Test
	public void testaInstanciaDependente(){
		DateTime data = new DateTime(1990, 03, 15, 00, 00);
		Dependente dependente = new Dependente("Jo�o", data);
		assertEquals(27, dependente.getIdade());
	}
	
	@Test
	public void testaInstanciaCustoDependenteEmDependente(){
		Dependente dependente = new Dependente("Carlos", new DateTime(1995, 03, 22, 00, 00));
		assertEquals(dependente.getValor(),new CustoDependenteMedio().getValor());
	}

}
