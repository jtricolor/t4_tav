package tests.entidade;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Test;

import entidade.CustoDependente;
import entidade.CustoDependenteJovem;
import entidade.CustoDependenteSenior;
import entidade.Dependente;

public class TestaCustoDependente {

	@Test
	public void instanciaCustoDependencia() {
		Dependente dependente = new Dependente("Jos�", new DateTime(2010,02,18,00,00));
		Dependente dependente2 = new Dependente("Carlos", new DateTime(1920,02,18,00,00));
		CustoDependente custo = new CustoDependenteJovem();
		CustoDependente custo2 = new CustoDependenteSenior();
		assertEquals(custo.getValor(), dependente.getCusto().getValor());
		assertEquals(custo2.getValor(), dependente2.getCusto().getValor());
	}

}
