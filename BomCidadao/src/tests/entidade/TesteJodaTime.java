package tests.entidade;

import static org.junit.Assert.*;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.junit.Test;

import Utils.JodaTimeUtils;

public class TesteJodaTime {

	@Test
	public void TestaCalculaIdade(){
		DateTime dataNascimento = new DateTime(1979, 02, 15, 00, 00); 
		JodaTimeUtils joda = new JodaTimeUtils();
		assertEquals(38, joda.calculaIdade(dataNascimento));
	}
	
	@Test
	public void TestaDataAtual(){
		DateMidnight hoje = new DateMidnight();
		DateMidnight data = new DateMidnight(2017, 06, 18);
		assertEquals(hoje, data);
	}
	
	@Test
	public void umAnoDepois(){
		DateMidnight hoje = new DateMidnight();
		DateMidnight ano = hoje.plusYears(1);
		assertEquals(new DateMidnight(2018, 06, 18), ano);
	}
}
