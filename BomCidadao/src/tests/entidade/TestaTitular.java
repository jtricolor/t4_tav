package tests.entidade;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;

import entidade.Dependente;
import entidade.Titular;

public class TestaTitular {
	
	@Test
	public void TestaBomCidadao() {
		Titular titular = new Titular(1,"Ana");
		titular.calcularCustoDependentes();
	}
	
	@Test
	public void testaCalcularCustoDependentes(){
		Titular titular = new Titular(1,"Ana");
		Dependente dependente1 = new Dependente("Pedro", new DateTime(1980, 05, 10, 00, 00));
		Dependente dependente2 = new Dependente("Pedro", new DateTime(1990, 05, 20, 00, 00));
		Dependente dependente3 = new Dependente("Pedro", new DateTime(2000, 12, 25, 00, 00));
		List<Dependente>dependentes = new ArrayList<Dependente>();
		dependentes.add(dependente1);
		dependentes.add(dependente2);
		dependentes.add(dependente3);
		titular.setDependentes(dependentes);
		titular.calcularCustoDependentes();
		assertEquals(new BigDecimal(227), titular.calcularCustoDependentes());
	}

}
